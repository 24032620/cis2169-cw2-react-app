import React, { Component } from 'react';
import ModulesData from './modules.json'
import './App.css'

class addmodules extends Component {

  render() {
    return ( 
      <form action="https://api.jsonbin.io/b/5ea82fc066e603359fe02337" method="post"> 
        <h1>To add a module please enter required data below</h1>
        ID Code: <input type="text" name="idCode" /> <br />
        Module name: <input type="text" name="name" /> <br />
        Hours: <input type="text" name="hours" /> <br />
        Learning outcome 1: <input type="text" name="LO1" /> <br />
        Learning outcome 2: <input type="text" name="LO2" /> <br />
        Credits: <input type="text" name="credits" /> <br />
      <button type="submit">Submit</button>
      </form>
    )
  }
}

export default addmodules