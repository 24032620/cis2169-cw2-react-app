import React, { Component } from 'react';
import './App.css';

function Footer() {
    return (
        <footer>
            <p id="foot1">&copy; Shaun Halliday 2020</p>
            <p id="foot2">CIS2169 24032620 CW2 Academic Tracker</p>
        </footer> 
    );
}

export default Footer