import React, { Component } from 'react';
import TimeslotsData from './timeslots.json'

class timeslots extends Component {

  render() {
    return ( 
      <div>

      <h1>Time Slots</h1>

      {TimeslotsData.map((timeslotDetail, index)=>{
        return <div>
        <h1>Module: {timeslotDetail.Module}</h1>
        <p>Time: {timeslotDetail.Time}</p>
        <p>Week: {timeslotDetail.Week}</p>
        <p>Day: {timeslotDetail.Day}</p>
        <p>Academic: {timeslotDetail.Academic}</p>
        <p>Room: {timeslotDetail.Room}</p>
        </div>
      })}

      </div>
    )
  }
}

export default timeslots