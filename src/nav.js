import React, { Component } from 'react';
import './App.css';
import {Link} from 'react-router-dom';

function Nav() {

    return (
        <nav>
            <ul>
                <Link to='/modules'>
                <li>Show Modules</li>
                </Link>
                <Link to='/addmodules'>
                <li>Add Modules</li>
                </Link>
                <Link  to='/degrees'>
                <li>Show Degrees</li>
                </Link>
                <Link  to='/adddegrees'>
                <li>Add Degrees</li>
                </Link>
                <Link  to='/assessments'>
                <li>Show Assessments</li>
                </Link>
                <Link  to='/addassessments'>
                <li>Add Assessments</li>
                </Link>
                <Link  to='/timeslots'>
                <li>Show Time Slots</li>
                </Link>
                <Link  to='/addtimeslots'>
                <li>Add Time Slots</li>
                </Link>
            </ul>
        </nav> 
    );
}

export default Nav