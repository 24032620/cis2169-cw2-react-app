import React, { Component } from 'react';
import AssessmentsData from './assessments.json'
import './App.css'

class addassessments extends Component {

  render() {
    return ( 
      
      <form action="https://api.jsonbin.io/b/5ea82fc066e603359fe02337" method="post"> 
        <h1>To add an assessment please enter required data below</h1>
        Name: <input type="text" name="name" /> <br />
        Number: <input type="text" name="number" /> <br />
        Learning outcomes: <input type="text" name="LearningOutcomes" /> <br />
        Volume: <input type="text" name="Volume" /> <br />
        Weighting: <input type="text" name="Weighting" /> <br />
        Submission Date: <input type="date" name="SubmissionDate" /> <br />
      <button type="submit">Submit</button>
      </form>
    )
  }
}

export default addassessments