/*
* import all required js files and css so can be implemented as required,
* also import router requirements to enable routing
*/
import React, { Component } from 'react';
import moduleslist from './modules';
import degrees from './degrees';
import assessments from './assessments';
import timeslots from './timeslots';
import addmodules from './addmodules';
import adddegrees from './adddegrees';
import addassessments from './addassessments';
import addtimeslots from './addtimeslots';
import './App.css';
import Header from './header';
import Footer from './footer';
import Nav from './nav';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

/* 
* Creation of main component, then render and return. Next Wrap all contents on Router
* to enable routing on all components, all routes also wrapped in switch to enable the
* switch between actual components so only one displayed at a time, nav and footer
* outside of this switch so always displayed in the app
*/

class App extends Component {
  render() {
    return (
      <Router>
      <div className ="App">
      <Header/>
      <Nav/>
      <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/modules" component={moduleslist}/>
      <Route path="/addmodules" component={addmodules}/>
      <Route path="/degrees" component={degrees}/>
      <Route path="/adddegrees" component={adddegrees}/>
      <Route path="/assessments" component={assessments}/>
      <Route path="/addassessments" component={addassessments}/>
      <Route path="/timeslots" component={timeslots}/>
      <Route path="/addtimeslots" component={addtimeslots}/>
      </Switch>
      <Footer />
      </div>
      </Router> 
    )
  }
}

/* 
* constant set for home page, returns heading and text on home page, this
* saves having a seperate file for only two pieces of text
*/
const Home = () => (
  <div>
    <h1>Home Page</h1>
    <p>Please navigate to your required area using the navigation above</p>
  </div>
)
//export so that App is able to be actioned
export default App