import React, { Component } from 'react';
import ModulesData from './modules.json'
import './App.css'

class modules extends Component {

  render() {
    return ( 
      
      <div>

      <h1>Modules</h1>

      {ModulesData.map((moduleDetail, index)=>{
        return <div>
          <card>
        <h1>{moduleDetail.idCode}</h1>
        <p>Name of module: {moduleDetail.name}</p>
        <p>Number of hours: {moduleDetail.hours}</p>
        <p>Learning outcome 1: {moduleDetail.LO1}</p>
        <p>Learning outcome 2: {moduleDetail.LO2}</p>
        <p>Credits: {moduleDetail.credits}</p>
        </card>
        </div>
      })}

      </div>
    )
  }
}

export default modules