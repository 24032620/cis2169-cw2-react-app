import React, { Component } from 'react';
import AssessmentsData from './assessments.json'

class assessments extends Component {

  render() {
    return ( 
      <div>

      <h1>Assessments</h1>

      {AssessmentsData.map((assessmentDetail, index)=>{
        return <div>
        <h1>Assessment: {assessmentDetail.name}</h1>
        <p>Assessment number: {assessmentDetail.number}</p>
        <p>Learning outcomes: {assessmentDetail.LearningOutcomes}</p>
        <p>Volume: {assessmentDetail.Volume}</p>
        <p>Weighting: {assessmentDetail.Weighting}</p>
        <p>Submission Date: {assessmentDetail.SubmissionDate}</p>
        </div>
      })}

      </div>
    )
  }
}

export default assessments