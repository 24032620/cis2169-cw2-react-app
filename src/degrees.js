import React, { Component } from 'react';
import DegreesData from './degrees.json'

class degrees extends Component {

  render() {
    return (
      <div>

        <h1>Degrees</h1>

        {DegreesData.map((degreeDetail, index) => {
          return <div>
            <h1>{degreeDetail.idCode}</h1>
            <p>Name of degree: {degreeDetail.name}</p>
            <p>Learning outcomes: {degreeDetail.LearningOutcomes}</p>
            <p>Exit Awards: {degreeDetail.ExitAwards}</p>
            <p>Credits: {degreeDetail.credits}</p>
          </div>
        })}

      </div>
    )
  }
}

export default degrees