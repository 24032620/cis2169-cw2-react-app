import React, { Component } from 'react';
import DegreesData from './degrees.json'
import './App.css'

class adddegrees extends Component {

  render() {
    return ( 

      <form action="https://api.jsonbin.io/b/5ea82fc066e603359fe02337" method="post"> 
        <h1>To add a degree please enter required data below</h1>
        ID Code: <input type="text" name="idCode" /> <br />
        Degree name: <input type="text" name="name" /> <br />
        Learning outcomes: <input type="text" name="LearningOutcomes" /> <br />
        Exit Awards: <input type="text" name="ExitAwards" /> <br />
        Credits: <input type="text" name="credits" /> <br />
      <button type="submit">Submit</button>
      </form>
    )
  }
}

export default adddegrees